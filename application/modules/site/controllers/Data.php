<?php
class Data extends MY_Controller {

  public function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
  }

  public function index() {
    $data['title'] = "Realisasi Belanja";
    $this->template->load('backend', 'data/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdPuskesmas = !empty($_POST['idPuskesmas'])?$_POST['idPuskesmas']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_BELTANGGAL=>'desc');
    $orderables = array(null,COL_BELTANGGAL,COL_BRGNAMA,null,null,null);
    $cols = array(COL_BRGNAMA,COL_BELCATATAN,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TBELANJA.".".COL_CREATEDBY,"left")
    ->join(TBL_MBARANG,TBL_MBARANG.'.'.COL_UNIQ." = ".TBL_TBELANJA.".".COL_IDBARANG,"left")
    ->group_by(COL_BELREFNO, COL_BELTANGGAL)
    ->get(TBL_TBELANJA);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_BRGNAMA) $item = TBL_MBARANG.'.'.COL_BRGNAMA;
      if($item == COL_CREATEDBY) $item = TBL_TBELANJA.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TBELANJA.'.'.COL_BELTANGGAL.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TBELANJA.'.'.COL_BELTANGGAL.' <= ', $dateTo);
    }
    if(!empty($IdPuskesmas)) {
      $this->db->where(TBL_TBELANJA.'.'.COL_IDUNIT, $IdPuskesmas);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tbelanja.*, mbarang.BrgNama, uc.Name as Nm_CreatedBy, sum(BelTotal) as GrandTotal, count(IdBarang) as NumItem')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TBELANJA.".".COL_CREATEDBY,"left")
    ->join(TBL_MBARANG,TBL_MBARANG.'.'.COL_UNIQ." = ".TBL_TBELANJA.".".COL_IDBARANG,"left")
    ->group_by(COL_BELREFNO, COL_BELTANGGAL)
    ->order_by(TBL_TBELANJA.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TBELANJA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/data/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="far fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/data/edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui"><i class="far fa-search"></i></a>&nbsp;';

      $_ket = strlen($r[COL_BELCATATAN]) > 50 ? substr($r[COL_BELCATATAN], 0, 50) . "..." : $r[COL_BELCATATAN];
      $data[] = array(
        $htmlBtn,
        date('Y-m-d', strtotime($r[COL_BELTANGGAL])),
        $r[COL_BELREFNO],
        number_format($r['NumItem']),
        number_format($r['GrandTotal']),
        $r[COL_CREATEDBY],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    $data['title'] = "Form Realisasi Belanja";

    if(!empty($_POST)) {
      $periodFrom = GetSetting('SETTING_PERIOD_FROM');
      $periodTo = GetSetting('SETTING_PERIOD_TO');

      if(strtotime($this->input->post(COL_BELTANGGAL))<strtotime($periodFrom) || strtotime($this->input->post(COL_BELTANGGAL))>strtotime($periodTo)) {
        ShowJsonError('Maaf, pembaruan tidak dapat dilakukan dikarenakan periode penginputan tanggal '.$this->input->post(COL_BELTANGGAL).' telah ditutup.');
        exit();
      }

      $arrIdBarang = $this->input->post('IdBarang[]');
      $arrVolume = $this->input->post('BelVolume[]');
      $arrSatuan = $this->input->post('BelSatuan[]');
      $arrTotal = $this->input->post('BelTotal[]');
      $arrCatatan = $this->input->post('BelCatatan[]');

      /*$det = array();
      $data = array(
        COL_IDUNIT => $this->input->post(COL_IDUNIT),
        COL_BELTANGGAL => $this->input->post(COL_BELTANGGAL),
        COL_BELREFNO => $this->input->post(COL_BELREFNO),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s'),
      );*/
      $data = array();
      $idUnit = $this->input->post(COL_IDUNIT);

      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $idUnit=$ruser[COL_COMPANYID];
      }

      if(!empty($arrIdBarang)) {
        for($i=0; $i<count($arrIdBarang); $i++) {
          $data[] = array(
            COL_IDUNIT => $idUnit,
            COL_BELTANGGAL => $this->input->post(COL_BELTANGGAL),
            COL_BELREFNO => $this->input->post(COL_BELREFNO),
            COL_CREATEDBY => $ruser[COL_USERNAME],
            COL_CREATEDON => date('Y-m-d H:i:s'),

            COL_IDBARANG=>$arrIdBarang[$i],
            COL_BELVOLUME=>$arrVolume[$i],
            COL_BELSATUAN=>$arrSatuan[$i],
            COL_BELTOTAL=>$arrTotal[$i],
            COL_BELCATATAN=>$arrCatatan[$i]
          );
        }
      }

      if(!empty($data)) {
        $this->db->trans_begin();
        try {
          $res = $this->db->insert_batch(TBL_TBELANJA, $data);
          if(!$res) {
            throw new Exception('Maaf, telah terjadi kesalahan sistem. Hubungi administrator untuk info lebih lanjut.');
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data berhasil.', array('redirect'=>site_url('site/data/index')));
        exit();
      } else {
        ShowJsonError('Rincian Belanja tidak boleh kosong!');
        exit();
      }
    } else {
      $this->template->load('backend', 'data/form', $data);
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    $data['title'] = "Form Realisasi Belanja";
    $data['data'] = $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TBELANJA)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $data['det'] = $rdet = $this->db
    ->select('tbelanja.*, mbarang.BrgNama')
    ->join(TBL_MBARANG,TBL_MBARANG.'.'.COL_UNIQ." = ".TBL_TBELANJA.".".COL_IDBARANG,"left")
    ->where(COL_BELREFNO, $rdata[COL_BELREFNO])
    ->where(COL_BELTANGGAL, $rdata[COL_BELTANGGAL])
    ->get(TBL_TBELANJA)
    ->result_array();

    if(!empty($_POST)) {
      $periodFrom = GetSetting('SETTING_PERIOD_FROM');
      $periodTo = GetSetting('SETTING_PERIOD_TO');

      if(strtotime($rdata[COL_BELTANGGAL])<strtotime($periodFrom) || strtotime($rdata[COL_BELTANGGAL])>strtotime($periodTo)) {
        ShowJsonError('Maaf, pembaruan tidak dapat dilakukan dikarenakan periode penginputan tanggal '.$rdata[COL_BELTANGGAL].' telah ditutup.');
        exit();
      }

      if(strtotime($this->input->post(COL_BELTANGGAL))<strtotime($periodFrom) || strtotime($this->input->post(COL_BELTANGGAL))>strtotime($periodTo)) {
        ShowJsonError('Maaf, pembaruan tidak dapat dilakukan dikarenakan periode penginputan tanggal '.$this->input->post(COL_BELTANGGAL).' telah ditutup.');
        exit();
      }

      $arrIdBarang = $this->input->post('IdBarang[]');
      $arrVolume = $this->input->post('BelVolume[]');
      $arrSatuan = $this->input->post('BelSatuan[]');
      $arrTotal = $this->input->post('BelTotal[]');
      $arrCatatan = $this->input->post('BelCatatan[]');

      $data = array();
      $idUnit = $this->input->post(COL_IDUNIT);

      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $idUnit=$ruser[COL_COMPANYID];
      }

      if(!empty($arrIdBarang)) {
        for($i=0; $i<count($arrIdBarang); $i++) {
          $data[] = array(
            COL_IDUNIT => $idUnit,
            COL_BELTANGGAL => $this->input->post(COL_BELTANGGAL),
            COL_BELREFNO => $this->input->post(COL_BELREFNO),
            COL_CREATEDBY => $rdata[COL_CREATEDBY],
            COL_CREATEDON => $rdata[COL_CREATEDON],
            COL_UPDATEDBY => $ruser[COL_USERNAME],
            COL_UPDATEDON => date('Y-m-d H:i:s'),

            COL_IDBARANG=>$arrIdBarang[$i],
            COL_BELVOLUME=>$arrVolume[$i],
            COL_BELSATUAN=>$arrSatuan[$i],
            COL_BELTOTAL=>$arrTotal[$i],
            COL_BELCATATAN=>$arrCatatan[$i]
          );
        }
      }

      if(!empty($data)) {
        $this->db->trans_begin();
        try {
          $res = $this->db
          ->where(COL_BELREFNO, $rdata[COL_BELREFNO])
          ->where(COL_BELTANGGAL, $rdata[COL_BELTANGGAL])
          ->delete(TBL_TBELANJA);
          if(!$res) {
            throw new Exception('Maaf, telah terjadi kesalahan sistem. Hubungi administrator untuk info lebih lanjut.');
          }

          $res = $this->db->insert_batch(TBL_TBELANJA, $data);
          if(!$res) {
            throw new Exception('Maaf, telah terjadi kesalahan sistem. Hubungi administrator untuk info lebih lanjut.');
          }
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data berhasil.', array('redirect'=>site_url('site/data/index')));
        exit();
      } else {
        ShowJsonError('Rincian Belanja tidak boleh kosong!');
        exit();
      }
    } else {
      $this->template->load('backend', 'data/form', $data);
    }
  }

  public function delete($id) {
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TBELANJA)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid.');
      exit();
    }

    $res = $this->db
    ->where(COL_BELREFNO, $rdata[COL_BELREFNO])
    ->where(COL_BELTANGGAL, $rdata[COL_BELTANGGAL])
    ->delete(TBL_TBELANJA);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Data berhasil dihapus.');
  }

  public function rekapitulasi() {
    $data['title'] = "Rekapitulasi Belanja";
    $cetak = false;

    if(!empty($_GET)) {
      $from = $this->input->get("dateFrom") ? $this->input->get("dateFrom") : date('m');
      $to = $this->input->get("dateTo") ? $this->input->get("dateTo") : date('Y');
      $puskesmas = $this->input->get("idPuskesmas") ? $this->input->get("idPuskesmas") : null;
      $cetak = $this->input->get("cetak") ? $this->input->get("cetak") : false;

      if(!empty($from)) {
        $this->db->where(TBL_TBELANJA.'.'.COL_BELTANGGAL.' >= ', $from);
      }
      if(!empty($to)) {
        $this->db->where(TBL_TBELANJA.'.'.COL_BELTANGGAL.' <= ', $to);
      }
      if(!empty($puskesmas)) {
        $this->db->where(TBL_TBELANJA.'.'.COL_IDUNIT, $puskesmas);
      }

      $data['res'] = $this->db
      ->select('mbarang.BrgNama, mbarang.IDBelanja, count(*) as BelCount, sum(tbelanja.BelTotal) as BelTotal')
      ->join(TBL_MBARANG,TBL_MBARANG.'.'.COL_UNIQ." = ".TBL_TBELANJA.".".COL_IDBARANG,"left")
      ->order_by(COL_BELTANGGAL, 'desc')
      ->group_by(TBL_MBARANG.'.'.COL_BRGNAMA, 'desc')
      ->get(TBL_TBELANJA)
      ->result_array();

      if(!empty($from)) {
        $this->db->where(TBL_TBELANJA.'.'.COL_BELTANGGAL.' >= ', $from);
      }
      if(!empty($to)) {
        $this->db->where(TBL_TBELANJA.'.'.COL_BELTANGGAL.' <= ', $to);
      }
      if(!empty($puskesmas)) {
        $this->db->where(TBL_TBELANJA.'.'.COL_IDUNIT, $puskesmas);
      }

      $data['rbelanja'] = $this->db
      ->select('mbelanja.BelNama, mbelanja.Uniq')
      ->join(TBL_MBARANG,TBL_MBARANG.'.'.COL_UNIQ." = ".TBL_TBELANJA.".".COL_IDBARANG,"left")
      ->join(TBL_MBELANJA,TBL_MBELANJA.'.'.COL_UNIQ." = ".TBL_MBARANG.".".COL_IDBELANJA,"left")
      ->order_by(COL_BELTANGGAL, 'desc')
      ->group_by(TBL_MBELANJA.'.'.COL_BELNAMA, 'desc')
      ->get(TBL_TBELANJA)
      ->result_array();
    }

    $data['cetak'] = $cetak;

    if($cetak) $this->load->view('site/data/rekapitulasi_', $data);
    else $this->template->load('backend', 'site/data/rekapitulasi', $data);

  }

  public function setting() {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      redirect('site/user/dashboard');
    }

    $data['title'] = "Pengaturan Periode";
    if(!empty($_POST)) {
      SetSetting('SETTING_PERIOD_FROM', $this->input->post('DateFrom'));
      SetSetting('SETTING_PERIOD_TO', $this->input->post('DateTo'));
      ShowJsonSuccess('Pembaruan pengaturan berhasil.');
    } else {
      $this->template->load('backend', 'site/data/setting', $data);
    }
  }
}
