<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> DASHBOARD</a></li>
          <li class="breadcrumb-item active">LAPORAN</li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header pt-4 pb-4">
              <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group row mb-0">
                    <label class="control-label col-sm-4">PUSKESMAS</label>
                    <div class="col-sm-8">
                      <?php
                      if($ruser[COL_ROLEID]==ROLEADMIN) {
                        ?>
                        <select class="form-control" name="idPuskesmas" style="width: 100%" required>
                          <?=GetCombobox("SELECT * FROM munit ORDER BY UnitNama", COL_UNIQ, COL_UNITNAMA, (!empty($_GET['idPuskesmas'])?$_GET['idPuskesmas']:null))?>
                        </select>
                        <?php
                      } else {
                        $rpuskesmas = $this->db
                        ->where(COL_UNIQ, $ruser[COL_COMPANYID])
                        ->get(TBL_MUNIT)
                        ->row_array();
                        ?>
                        <input type="text" class="form-control" value="<?=!empty($rpuskesmas)?$rpuskesmas[COL_UNITNAMA]:'--'?>" disabled />
                        <input type="hidden" name="idPuskesmas" value="<?=$ruser[COL_COMPANYID]?>" />
                        <?php
                      }
                      ?>
                      </div>
                  </div>
                </div>
                <div class="col-sm-5">
                  <div class="form-group row mb-0">
                    <label class="control-label col-sm-4">PERIODE</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control datepicker text-right" name="dateFrom" value="<?=date('Y-m-1')?>" />
                    </div>
                    <div class="col-sm-4">
                      <input type="text" class="form-control datepicker text-right" name="dateTo" value="<?=date('Y-m-d')?>" />
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <button type="submit" class="btn btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                  <button type="submit" class="btn btn-outline-success" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> CETAK</button>
                </div>
              </div>
              <?=form_close()?>
            </div>
            <div class="card-body">
              <?php
              if(!empty($res)) {
                $this->load->view('site/data/rekapitulasi_');
              }
              ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
