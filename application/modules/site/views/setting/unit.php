<?php $data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_UNIQ] . '" />',
        $d[COL_UNITTIPE],
        anchor('site/setting/unit-edit/'.$d[COL_UNIQ],$d[COL_UNITNAMA]),
        $d[COL_UNITPIMPINAN]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?= strtoupper($title) ?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <p class="mb-0">
          <?=anchor('site/setting/unit-delete','<i class="far fa-trash"></i> HAPUS',array('class'=>'cekboxaction btn btn-danger btn-sm','data-confirm'=>'Apa anda yakin?'))?>
          <?=anchor('site/setting/unit-add','<i class="far fa-plus"></i> TAMBAH',array('class'=>'btn btn-primary btn-sm'))?>
        </p>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover"></table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#datalist').dataTable({
      "autoWidth":false,
      //"sDom": "Rlfrtip",
      "aaData": <?=$data?>,
      //"bJQueryUI": true,
      //"aaSorting" : [[5,'desc']],
      "scrollY" : '48vh',
      "iDisplayLength": 100,
      "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      "order": [[ 2, "asc" ]],
      "columnDefs": [
        {"targets":[0,1,2], "className":'nowrap'},
        {"targets":[2], "className":'nowrap dt-body-right'}
      ],
      "aoColumns": [
          {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
          {"sTitle": "TIPE","sWidth":"10px","bSortable":false},
          {"sTitle": "NAMA"},
          {"sTitle": "PIMPINAN"}
      ]
    });
    $('#cekbox').click(function(){
        if($(this).is(':checked')){
            $('.cekbox').prop('checked',true);
        }else{
            $('.cekbox').prop('checked',false);
        }
    });
});
</script>
