<?php
$ruser = GetLoggedUser();
$runit = $this->db->count_all_results(TBL_MUNIT);
if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where(TBL_TBELANJA.'.'.COL_IDUNIT, $ruser[COL_COMPANYID]);
}
$this->db->where('YEAR(BelTanggal)', date('Y'));
$cbelanja = $this->db->count_all_results(TBL_TBELANJA);

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where(TBL_TBELANJA.'.'.COL_IDUNIT, $ruser[COL_COMPANYID]);
}
$this->db->where('YEAR(BelTanggal)', date('Y'));
$tbelanja = $this->db->select_sum(COL_BELTOTAL)->get(TBL_TBELANJA)->row_array();
 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?= strtoupper($title) ?></h3>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-info">
            <div class="inner">
              <h3><?=number_format($runit)?></h3>

              <p>Unit / Puskesmas</p>
            </div>
            <div class="icon">
              <i class="far fa-building"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=number_format($cbelanja)?></h3>

              <p>Realisasi Belanja TA. <strong><?=date('Y')?></strong></p>
            </div>
            <div class="icon">
              <i class="far fa-database"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?=number_format($tbelanja[COL_BELTOTAL])?></h3>

              <p>Total Belanja TA. <strong><?=date('Y')?></strong></p>
            </div>
            <div class="icon">
              <i class="far fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <?php
      } else {
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-success">
            <div class="inner">
              <h3><?=number_format($cbelanja)?></h3>

              <p>Realisasi Belanja TA. <strong><?=date('Y')?></strong></p>
            </div>
            <div class="icon">
              <i class="far fa-database"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-danger">
            <div class="inner">
              <h3><?=number_format($tbelanja[COL_BELTOTAL])?></h3>

              <p>Total Belanja TA. <strong><?=date('Y')?></strong></p>
            </div>
            <div class="icon">
              <i class="far fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <?php
      }
      ?>

    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
});
</script>
